# Paclair

Paclair on alpine linux with an extra script to adopt it to GitLab container scanning.  
It's built for amd64 and aarch64 via GitLab CI.

## Image content

### Paclair

Paclair is a tool (CLI) which interacts with [Clair](https://github.com/coreos/clair), which in turn is a tool for container scanning.  
If you use a remote clair server which you need to access, this is the tool you are looking for!

### GitLabConvert

GitLabConvert is a convenience script written in python to convert the output of paclair to fit the expected format of 
GitLab CI container scan reports.  
The script is written by Johannes Tegnér and can be found [here](https://gist.githubusercontent.com/Johannestegner/cedb46c87780ffbbd7f3fbbd86add1d4).

## Usage

```
# Clair commands.
docker run --rm jitesoft/paclair paclair --help
# For gitlab conversion.
docker run --rm jitesoft/paclair GitLabConvert image:tag clair-report.json gl-container-scanning-report.json
```

Check [Paclair](https://github.com/yebinama/paclair) for further information on how to use the application. 

## Image labels

This image follows the [Jitesoft image label specification 1.0.0](https://gitlab.com/snippets/1866155).

## License

Paclair is released under the [`Apache License 2.0`](https://github.com/yebinama/paclair/blob/master/LICENSE).  
The files in this repository are released under the [MIT license](https://gitlab.com/jitesoft/dockerfiles/php/blob/master/LICENSE).  
The GitLabConvert python script is released under the [MIT license](https://gitlab.com/jitesoft/dockerfiles/php/blob/master/LICENSE).  
