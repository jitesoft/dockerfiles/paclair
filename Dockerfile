FROM registry.gitlab.com/jitesoft/dockerfiles/python:3
ARG VERSION
ARG CONVERTER_SHA
LABEL maintainer="Johannes Tegnér <johannes@jitesoft.com>" \
      maintainer.org="Jitesoft" \
      maintainer.org.uri="https://jitesoft.com" \
      com.jitesoft.project.repo.type="git" \
      com.jitesoft.project.repo.uri="https://gitlab.com/jitesoft/dockerfiles/paclair" \
      com.jitesoft.project.repo.issues="https://gitlab.com/jitesoft/dockerfiles/paclair/issues" \
      com.jitesoft.project.registry.uri="registry.gitlab.com/jitesoft/dockerfiles/paclair" \
      com.jitesoft.app.paclair.version="${VERSION}" \
      com.jitesoft.app.converter.version="${CONVERTER_SHA}"

ARG VERSION
ARG CONVERTER_SHA

RUN apk add --no-cache --virtual .dep curl tar \
 && curl -LsS https://gist.githubusercontent.com/Johannestegner/cedb46c87780ffbbd7f3fbbd86add1d4/raw/${CONVERTER_SHA}/GitlabConvert.py -o /usr/local/bin/GitLabConvert \
 && curl -Ls https://github.com/yebinama/paclair/archive/v${VERSION}.tar.gz | tar -xz \
 && cd paclair-${VERSION} \
 && pip install . \
 && cd .. \
 && rm -rf paclair-${VERSION} \
 && apk del .dep \
 && chmod +x /usr/local/bin/GitLabConvert

CMD [ "paclair" ]
